#include "HealthComponent.h"

UHealthComponent::UHealthComponent()
{
	CurrentHealth = 100;
	MaxHealth = 100;
	bAutoActivate = true;
}

void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	GetOwner()->OnTakePointDamage.AddDynamic(this, &UHealthComponent::OnOwnerTakePointDamage);
	GetOwner()->OnTakeRadialDamage.AddDynamic(this, &UHealthComponent::OnOwnerTakeRadiusDamage);
}

void UHealthComponent::ApplyDamage(float BaseDamage, AController* EventInstigator, AActor* DamageCauser, TSubclassOf<UDamageType> DamageTypeClass = UDamageType::StaticClass())
{
	if (!DamageTypeClass)
		return;

	FVector HitDirection = DamageCauser ? GetOwner()->GetActorLocation() - DamageCauser->GetActorLocation() : FVector::ZeroVector;
	HitDirection.Normalize();
	
	ProcessDamage(BaseDamage, DamageTypeClass, EventInstigator, DamageCauser, GetOwner()->GetActorLocation(), HitDirection);
}

void UHealthComponent::ProcessDamage(float Damage, TSubclassOf<UDamageType> DamageTypeClass, AController* InstigatedBy, AActor* DamageCauser, FVector HitLocation, FVector HitDirection)
{
	if (CurrentHealth <= 0)
		return;

	const int32 OldAmount = CurrentHealth;
	CurrentHealth = FMath::Max(CurrentHealth - Damage, 0.f);
	if (OldAmount != CurrentHealth)
	{
		TakeDamage(Damage, DamageTypeClass, InstigatedBy, DamageCauser, HitLocation, HitDirection);
		OnDamageTaken.Broadcast(Damage, DamageTypeClass, InstigatedBy, DamageCauser, HitLocation, HitDirection);
	}

	if (CurrentHealth <= 0)
	{
		OnDied.Broadcast(DamageTypeClass, InstigatedBy, DamageCauser, HitLocation, HitDirection);
	}
}

void UHealthComponent::OnOwnerTakePointDamage(AActor* DamagedActor, float Damage, AController* InstigatedBy, FVector HitLocation, UPrimitiveComponent* FHitComponent, FName BoneName, FVector ShotFromDirection, const UDamageType* DamageType, AActor* DamageCauser)
{
	ShotFromDirection.Normalize();
	ProcessDamage(Damage, DamageType->GetClass(), InstigatedBy, DamageCauser, HitLocation, ShotFromDirection);
}

void UHealthComponent::OnOwnerTakeRadiusDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, FVector Origin, FHitResult HitInfo, AController* InstigatedBy, AActor* DamageCauser)
{
	FVector ShotFromDir = DamageCauser ? DamagedActor->GetActorLocation() - DamageCauser->GetActorLocation() : FVector::ZeroVector;
	ShotFromDir.Normalize();
	ProcessDamage(Damage, DamageType->GetClass(), InstigatedBy, DamageCauser, HitInfo.ImpactPoint, ShotFromDir);
}

void UHealthComponent::AddHealth(int32 Amount)
{
	if (Amount <= 0)
		return;

	CurrentHealth = FMath::Min(CurrentHealth + Amount, MaxHealth);
}

