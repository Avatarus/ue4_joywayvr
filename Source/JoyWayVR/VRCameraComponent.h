#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraComponent.h"
#include "VRCameraComponent.generated.h"

/**
 * 
 */
UCLASS(meta = (BlueprintSpawnableComponent))
class JOYWAYVR_API UVRCameraComponent : public UCameraComponent
{
	GENERATED_BODY()

	UVRCameraComponent(const FObjectInitializer& ObjectInitializer);

private:
	FVector LastPosition;

	virtual void GetCameraView(float DeltaTime, FMinimalViewInfo& DesiredView) override;
};
