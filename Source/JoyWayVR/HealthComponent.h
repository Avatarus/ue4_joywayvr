#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DamageType.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_SixParams(FHealthDamageTaken, float, Damage, TSubclassOf<UDamageType>, DamageTypeClass, class AController*, InstigatedBy, AActor*, DamageCauser, FVector, HitLocation, FVector, HitDirection);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_FiveParams(FHealthDied, TSubclassOf<UDamageType>, DamageTypeClass, class AController*, InstigatedBy, AActor*, DamageCauser, FVector, HitLocation, FVector, HitDirection);

UCLASS(Blueprintable, BlueprintType, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class JOYWAYVR_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UHealthComponent();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Health")
	int32 CurrentHealth;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Health")
	int32 MaxHealth;

	UPROPERTY(BlueprintAssignable, Category = "Health")
	FHealthDamageTaken OnDamageTaken;

	UPROPERTY(BlueprintAssignable, Category = "Health")
	FHealthDied OnDied;

	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly, Category = "Health")
	void ApplyDamage(float BaseDamage, AController* EventInstigator, AActor* DamageCauser, TSubclassOf<class UDamageType> DamageTypeClass);

	UFUNCTION(BlueprintImplementableEvent)
	void TakeDamage(float Damage, TSubclassOf<UDamageType> DamageTypeClass, class AController* InstigatedBy, AActor* DamageCauser, FVector HitLocation, FVector HitDirection);

	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool IsFull() const { return CurrentHealth == MaxHealth; }

	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool IsDead() const { return CurrentHealth <= 0; }

	UFUNCTION(BlueprintCallable, Category = "Health")
	void AddHealth(int32 Amount);

protected:
	virtual void BeginPlay() override;

	virtual void ProcessDamage(float Damage, TSubclassOf<UDamageType> DamageTypeClass, class AController* InstigatedBy, AActor* DamageCauser, FVector HitLocation, FVector HitDirection);

private:
	UFUNCTION()
	void OnOwnerTakePointDamage(AActor* DamagedActor, float Damage, class AController* InstigatedBy, FVector HitLocation, class UPrimitiveComponent* FHitComponent, FName BoneName, FVector ShotFromDirection, const class UDamageType* DamageType, AActor* DamageCauser);

	UFUNCTION()
	void OnOwnerTakeRadiusDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, FVector Origin, FHitResult HitInfo, class AController* InstigatedBy, AActor* DamageCauser);
};
