#include "VRCameraComponent.h"
#include "IXRTrackingSystem.h"
#include "IXRCamera.h"

UVRCameraComponent::UVRCameraComponent(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	//bLockToHmd = false;
}

void UVRCameraComponent::GetCameraView(float DeltaTime, FMinimalViewInfo& DesiredView)
{
	Super::GetCameraView(DeltaTime, DesiredView);
	
	if (GEngine && GEngine->XRSystem.IsValid() && GetWorld() && GetWorld()->WorldType != EWorldType::Editor)
	{
		IXRTrackingSystem* XRSystem = GEngine->XRSystem.Get();
		auto XRCamera = XRSystem->GetXRCamera();

		if (XRCamera.IsValid())
		{
			if (XRSystem->IsHeadTrackingAllowedForWorld(*GetWorld()))
			{
				const FTransform ParentWorld = CalcNewComponentToWorld(FTransform());

				XRCamera->SetupLateUpdate(ParentWorld, this, false);

				FQuat Orientation;
				FVector Position;
				if (XRCamera->UpdatePlayerCamera(Orientation, Position))
				{
					AActor* OwnerActor = GetOwner();
					if (OwnerActor)
					{
						if (LastPosition != FVector::ZeroVector)
						{
							FVector Offset = OwnerActor->GetActorRotation().RotateVector(Position - LastPosition) * FVector(1.f, 1.f, 0.f);

							// Add camera position delta to actor location
							OwnerActor->AddActorWorldOffset(Offset, true);
						}
						// Cache camera position
						LastPosition = Position;
					}

					// Apply Z position and rotation to camera component
					SetRelativeTransform(FTransform(Orientation, FVector(0.f, 0.f, Position.Z)));
				}
				else
				{
					ResetRelativeTransform();
				}
				
			}
		}
	}

	if (!bUseAdditiveOffset)
	{
		DesiredView.Location = GetComponentLocation();
		DesiredView.Rotation = GetComponentRotation();
	}
}